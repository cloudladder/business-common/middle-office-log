# middle-office-log
古珀监控日志抓取sdk

## 来源
本包fork自 [itsgoingd/clockwork](https://github.com/itsgoingd/clockwork)，感谢作者。

## 使用
在原包的基础上，增加了对经过公司内部 cloudladder/http 包所发出的请求的日志监控。  
因此需要配合 cloudladder/http 使用。

## env配置

#### 是否开启commands收集
CLOCKWORK_ARTISAN_COLLECT=true
#### 是否开启queue收集
CLOCKWORK_QUEUE_COLLECT=true



