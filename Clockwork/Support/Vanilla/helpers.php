<?php

use Clockwork\Support\Vanilla\Clockwork;

if (! function_exists('clock')) {
	// Log a message to Clockwork, returns Clockwork instance when called with no arguments, first argument otherwise
	function clock(...$arguments)
	{
		if (empty($arguments)) {
			return Clockwork::instance();
		}

		foreach ($arguments as $argument) {
			Clockwork::debug($argument);
		}

		return reset($arguments);
	}
}

if (! function_exists('clock_user_data')) {
    function clock_user_data(string $title, array $counters = [], string $tableName = '', array $table = [])
    {
        if (! function_exists('clock')){
            return;
        }

        $clock = clock()->userData($title)->title(ucwords($title));

        if ($counters){
            $clock->counters($counters);
        }

        if ($table){
            $clock->table($tableName, $table);
        }
    }
}
