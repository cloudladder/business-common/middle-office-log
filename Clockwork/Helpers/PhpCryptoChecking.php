<?php

namespace Clockwork\Helpers;


/**
 * Class PhpCrypto
 *
 * @author: Wumeng - wumeng@gupo.onaliyun.com
 * @since: 2024-02-20 15:43
 */
class PhpCryptoChecking
{
    protected $hasPhpCrypto = false;

    private static $instance = null;

    /**
     *
     *
     * @return self|null
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2024-02-20 16:06
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        if (class_exists('Composer\InstalledVersions') && class_exists('Gupo\PhpCrypto\ParamCrypto')) {
            $packages = \Composer\InstalledVersions::getInstalledPackages();

            foreach ($packages as $package) {
                if ($package === 'gupo/php-crypto') {
                    $this->hasPhpCrypto = true;
                    break;
                }
            }
        }
    }

    public function getPhpCryptoFlg()
    {
        return $this->hasPhpCrypto;
    }

    /**
     * 解密国密传参
     *
     * @param $getOrPost
     * @return mixed
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2024-02-20 11:24
     */
    public function decryptPhpCryptoParams($getOrPost)
    {
        if ($this->getPhpCryptoFlg() && (isset($getOrPost['data']) && isset($getOrPost['key']))) {
            $cryptoCls = new \ReflectionClass('\Gupo\PhpCrypto\ParamCrypto');
            $instance = $cryptoCls->newInstance();
            $decryptParams = $cryptoCls->getMethod('decryptParams');
            $decryptParams->setAccessible(true);
            return $decryptParams->invokeArgs($instance, [$getOrPost['data'], $getOrPost['key']]);
        }
        return $getOrPost;
    }

    /**
     * 解密国密uri传参
     *
     * @param $str
     * @return mixed|string
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2024-02-20 14:40
     */
    public function decryptPhpCryptoUri($str)
    {
        if ($this->getPhpCryptoFlg() && mb_strlen($str) > 0) {
            $array = explode('?', $str);
            if (count($array) < 1){
                return $str;
            }
            $str = $array[0];
            if ($_GET){
                $params = $this->decryptPhpCryptoParams($_GET);
                if ($params){
                    return $str . '?' . http_build_query($params);
                }
            }
        }
        return $str;
    }
}
